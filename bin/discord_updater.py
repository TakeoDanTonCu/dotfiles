import json

JSON_PATH='/usr/share/discord/resources/build_info.json'

# Lire le contenu du fichier JSON
with open(JSON_PATH, 'r') as file:
    data = json.load(file)

# Incrémenter la version
version = data['version'].split('.')
major = int(version[0])
minor = int(version[1])
patch = int(version[2])

patch += 1  # Incrémenter le patch

# Reconstruire la version
new_version = f"{major}.{minor}.{patch}"

# Mettre à jour la version dans le fichier JSON
data['version'] = new_version

# Écrire les données mises à jour dans le fichier JSON
with open(JSON_PATH, 'w') as file:
    json.dump(data, file, indent=2)

print(f"Version incrémentée avec succès. Nouvelle version : {new_version}")
